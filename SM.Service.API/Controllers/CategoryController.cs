﻿using SM.Core.Entity;
using SM.Core.Interface.BusinessLayer;
using SM.Service.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SM.Service.API.Controllers
{
    public class CategoryController : BaseApiController
    {
        ICategoryBusiness _obj = null;
        public CategoryController(ICategoryBusiness obj)
        {
            _obj = obj;
        }

        //[Route("api/v1/category/")]
        public IEnumerable<CategoryEntity> Get()
        {
            return _obj.GetAll();
        }

        //[Route("api/v1/category/{id}")]
        public CategoryEntity Get(int id)
        {
            return _obj.SingleOrDefault(x => x.ID == id);
        }

        public HttpResponseMessage Post([FromBody]CategoryModel newCategory)
        {
            try
            {
                var entity = TModelFactory.Parse(newCategory);
                _obj.Add(entity);
                return Request.CreateResponse(HttpStatusCode.Created, TModelFactory.Create(entity));
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Something bad happened");
            }
        }

        public HttpResponseMessage Delete(int id)
        {
            try
            {
                var result = _obj.SingleOrDefault(x => x.ID == id);
                if (result == null)
                    return Request.CreateResponse(HttpStatusCode.NotFound);
                _obj.Delete(x => x.ID == id);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Something bad happened");
            }
        }

        [HttpPatch]
        [HttpPut]
        public HttpResponseMessage Put([FromBody]CategoryModel category)
        {
            try
            {
                var result = _obj.SingleOrDefault(x => x.ID == category.ID);
                if (result == null)
                    return Request.CreateResponse(HttpStatusCode.NotFound);

                result.CategoryName = category.CategoryName;

                _obj.Update(result);

                return Request.CreateResponse(HttpStatusCode.OK, TModelFactory.Create(result));
            }

            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Something bad happened");
            }
        }

    }
}
