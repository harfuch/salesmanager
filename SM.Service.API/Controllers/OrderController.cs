﻿using SM.Core.Entity;
using SM.Core.Interface.BusinessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SM.Service.API.Controllers
{
    public class OrderController : ApiController
    {
        IOrderBusiness _obj = null;
        public OrderController(IOrderBusiness obj)
        {
            _obj = obj;
        }

        [Route("api/v1/order/")]
        public IEnumerable<OrderEntity> Get()
        {
            return _obj.GetAll();
        }

        [Route("api/v1/order/{id}")]
        public OrderEntity Get(int id)
        {
            return _obj.SingleOrDefault(x => x.ID == id);
        }
    }
}
