﻿using SM.Service.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SM.Service.API.Controllers
{
    public abstract class BaseApiController : ApiController
    {
        ModelFactory _modelFactory;
        public BaseApiController()
        {
        }

        protected ModelFactory TModelFactory
        {
            get
            {
                if(_modelFactory == null)
                    _modelFactory = new ModelFactory(this.Request);
                return _modelFactory;
            }
        }
    }
}
