﻿using SM.Core.Entity;
using SM.Core.Interface.BusinessLayer;
using SM.Service.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SM.Service.API.Controllers
{
    public class BasketController : BaseApiController
    {
        IBasketBusiness _obj = null;
        public BasketController(IBasketBusiness obj)
        {
            _obj = obj;
        }

        public IEnumerable<BasketModel> Get()
        {
            return _obj.GetAll().Select(x => TModelFactory.Create(x));
        }

        public HttpResponseMessage Get(int id)
        {
            try
            {
                var result = _obj.SingleOrDefault(x => x.ID == id);
                if (result == null)
                    return Request.CreateResponse(HttpStatusCode.NotFound);
                return Request.CreateResponse(HttpStatusCode.OK, TModelFactory.Create(result));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        public HttpResponseMessage Post([FromBody]BasketModel basket)
        {
            try
            {
                var entity = TModelFactory.Parse(basket);
                var newBask = _obj.Add(entity);

                if (newBask != null)
                    return Request.CreateResponse(HttpStatusCode.Created, TModelFactory.Create(newBask));

                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Something bad happened");
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Something bad happened");
            }
        }

        /// <summary>
        /// Delete basket
        /// </summary>
        /// <param name="id">basket id</param>
        /// <returns>HttpResponseMessage</returns>
        public HttpResponseMessage Delete(int id)
        {
            try
            {
                var result = _obj.SingleOrDefault(x => x.ID == id);
                if (result == null)
                    return Request.CreateResponse(HttpStatusCode.NotFound);

                _obj.Delete(x => x.ID == result.ID);

                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch(Exception ex)
            {
                //return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Something bad happened");
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpPatch]
        [HttpPut]
        public HttpResponseMessage Put([FromBody]CategoryModel category)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "Use Post Method");
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Use Post Method");
            }
        }
    }
}
