﻿using SM.Core.Entity;
using SM.Core.Interface.BusinessLayer;
using SM.Service.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SM.Service.API.Controllers
{
    public class PaymentMethodController : BaseApiController
    {
        IPaymentMethodBusiness _obj = null;

        public PaymentMethodController(IPaymentMethodBusiness obj)
        {
            _obj = obj;
        }

        //[Route("api/v1/paymentmethod/")]
        public IEnumerable<PaymentMethodEntity> Get()
        {
            return _obj.GetAll();
        }

        //[Route("api/v1/paymentmethod/{id}")]
        public PaymentMethodEntity Get(int id)
        {
            return _obj.SingleOrDefault(x => x.ID == id);
        }

        public HttpResponseMessage Post([FromBody]PaymentMethodModel newPayMethod)
        {
            try
            {
                var entity = TModelFactory.Parse(newPayMethod);
                _obj.Add(entity);
                return Request.CreateResponse(HttpStatusCode.Created, TModelFactory.Create(entity));
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Something bad happened");
            }
        }

        public HttpResponseMessage Delete(int id)
        {
            try
            {
                var result = _obj.SingleOrDefault(x => x.ID == id);
                if (result == null)
                    return Request.CreateResponse(HttpStatusCode.NotFound);
                _obj.Delete(x => x.ID == id);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Something bad happened");
            }
        }

        [HttpPatch]
        [HttpPut]
        public HttpResponseMessage Put([FromBody]PaymentMethodModel payMethod)
        {
            try
            {
                var result = _obj.SingleOrDefault(x => x.ID == payMethod.ID);
                if (result == null)
                    return Request.CreateResponse(HttpStatusCode.NotFound);

                result.PaymentDescription = payMethod.PaymentDescription;

                _obj.Update(result);

                return Request.CreateResponse(HttpStatusCode.OK, TModelFactory.Create(result));
            }

            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Something bad happened");
            }
        }
    }
}
