﻿using SM.Core.Entity;
using SM.Core.Interface.BusinessLayer;
using SM.Service.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SM.Service.API.Controllers
{
    public class ProductController : BaseApiController
    {
        IProductBusiness _obj = null;
        public ProductController(IProductBusiness obj)
        {
            _obj = obj;
        }

        public IEnumerable<ProductModel> Get()
        {
            return _obj.GetAll().ToList().Select(f => TModelFactory.Create(f));
        }

        public HttpResponseMessage Get(int id)
        {
            var result = _obj.SingleOrDefault(x => x.ID == id);
            if (result == null)
                return Request.CreateResponse(HttpStatusCode.NotFound);
            return Request.CreateResponse(HttpStatusCode.OK, TModelFactory.Create(result));
        }

        [Route("api/v1/product/category/{idCategory}")]
        public IEnumerable<ProductModel> GetProductByCategory(int idCategory)
        {
            return _obj.GetAll(x => x.IdCategory == idCategory).ToList().Select(f => TModelFactory.Create(f));
        }

        public HttpResponseMessage Post([FromBody]ProductModel newProduct)
        {
            try
            {
                var entity = TModelFactory.Parse(newProduct);
                _obj.Add(entity);
                return Request.CreateResponse(HttpStatusCode.Created, TModelFactory.Create(entity));
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Something bad happened");
            }
        }

        public HttpResponseMessage Delete(int id)
        {
            try
            {
                var result = _obj.SingleOrDefault(x => x.ID == id);
                if (result == null)
                    return Request.CreateResponse(HttpStatusCode.NotFound);
                _obj.Delete(x => x.ID == id);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Something bad happened");
            }
        }

        [HttpPatch]
        [HttpPut]
        public HttpResponseMessage Put([FromBody]ProductModel product)
        {
            try
            {
                var result = _obj.SingleOrDefault(x => x.ID == product.ID);
                if (result == null)
                    return Request.CreateResponse(HttpStatusCode.NotFound);

                result.ProductPrice = product.ProductPrice;
                result.ProductName = product.ProductName;
                result.IdCategory = product.IdCategory;

                _obj.Update(result);

                return Request.CreateResponse(HttpStatusCode.OK, TModelFactory.Create(result));
            }

            catch
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Something bad happened");
            }
        }
    }
}
