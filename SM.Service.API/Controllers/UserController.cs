﻿using SM.Core.Entity;
using SM.Core.Interface.BusinessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SM.Service.API.Controllers
{
    public class UserController : ApiController
    {
        IUserBusiness _obj = null;
        public UserController(IUserBusiness obj)
        {
            _obj = obj;
        }

        [Route("api/v1/user/")]
        public IEnumerable<UserEntity> Get()
        {
            return _obj.GetAll();
        }

        [Route("api/v1/user/{id}")]
        public UserEntity Get(int id)
        {
            return _obj.SingleOrDefault(x => x.ID == id);
        }

        /// <summary>
        /// Alterar o Authentication
        /// </summary>
        /// <param name="login"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        [Route("api/v1/user/auth/{login}/{password}")]
        public UserEntity Authentication(string login, string password)
        {
            UserEntity user = new UserEntity()
            {
                Login = login,
                Password = password
            };
            return _obj.Authentication(user);
        }
    }
}
