﻿using SM.Core.Entity;
using SM.Core.Interface.BusinessLayer;
using SM.Service.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SM.Service.API.Controllers
{
    public class BasketItemController : BaseApiController
    {
        IBasketItemBusiness _obj = null;
        public BasketItemController(IBasketItemBusiness obj)
        {
            _obj = obj;
        }

        /// <summary>
        /// Delete a item from basket
        /// </summary>
        /// <param name="id">basket id</param>
        /// <returns>HttpResponseMessage</returns>
        //public HttpResponseMessage Delete([FromBody]BasketItemModel basketItem)
        //{
        //    try
        //    {
        //        if (basketItem == null)
        //            return Request.CreateResponse(HttpStatusCode.NotFound);

        //        var result = _obj.SingleOrDefault(x => x.IdBasket == basketItem.IdBasket && x.IdProduct == basketItem.IdProduct);
        //        if (result == null)
        //            return Request.CreateResponse(HttpStatusCode.NotFound);

        //        _obj.DeleteItem(TModelFactory.Parse(basketItem));

        //        return Request.CreateResponse(HttpStatusCode.OK);
        //    }
        //    catch (Exception ex)
        //    {
        //        //return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Something bad happened");
        //        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
        //    }
        //}

        public HttpResponseMessage Delete(int id)
        {
            try
            {
                var result = _obj.SingleOrDefault(x => x.ID == id);
                if (result == null)
                    return Request.CreateResponse(HttpStatusCode.NotFound);
                _obj.DeleteItem(result);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }
    }
}
