[assembly: WebActivator.PostApplicationStartMethod(typeof(SM.Service.API.App_Start.SimpleInjectorWebApiInitializer), "Initialize")]

namespace SM.Service.API.App_Start
{
    using System.Web.Http;
    using SimpleInjector;
    using SimpleInjector.Integration.WebApi;
    using SimpleInjector.Lifestyles;
    using Core.Interface.BusinessLayer;
    using Core.BusinessLayer;
    using Core.Interface.DataAccessLayer;
    using Core.DataAccessLayer;

    public static class SimpleInjectorWebApiInitializer
    {
        /// <summary>Initialize the container and register it as Web API Dependency Resolver.</summary>
        public static void Initialize()
        {
            //var container = new Container();
            //container.Options.DefaultScopedLifestyle = new WebApiRequestLifestyle();

            var container = new Container();
            container.Options.DefaultScopedLifestyle = new AsyncScopedLifestyle();

            InitializeContainer(container);

            container.RegisterWebApiControllers(GlobalConfiguration.Configuration);
       
            container.Verify();
            
            GlobalConfiguration.Configuration.DependencyResolver =
                new SimpleInjectorWebApiDependencyResolver(container);
        }

        private static void InitializeContainer(Container container)
        {
            container.Register<IUserBusiness, UserBusiness>(Lifestyle.Scoped);
            container.Register<IUserDataAccess, UserDataAccess>(Lifestyle.Scoped);
            container.Register<IProductBusiness, ProductBusiness>(Lifestyle.Scoped);
            container.Register<IProductDataAccess, ProductDataAccess>(Lifestyle.Scoped);
            container.Register<ICategoryBusiness, CategoryBusiness>(Lifestyle.Scoped);
            container.Register<ICategoryDataAccess, CategoryDataAccess>(Lifestyle.Scoped);
            container.Register<IPaymentMethodBusiness, PaymentMethodBusiness>(Lifestyle.Scoped);
            container.Register<IPaymentMethodDataAccess, PaymentMethodDataAccess>(Lifestyle.Scoped);
            container.Register<IOrderBusiness, OrderBusiness>(Lifestyle.Scoped);
            container.Register<IOrderDataAccess, OrderDataAccess>(Lifestyle.Scoped);
            container.Register<IBasketBusiness, BasketBusiness>(Lifestyle.Scoped);
            container.Register<IBasketDataAccess, BasketDataAccess>(Lifestyle.Scoped);
            container.Register<IBasketItemBusiness, BasketItemBusiness>(Lifestyle.Scoped);
            container.Register<IBasketItemDataAccess, BasketItemDataAccess>(Lifestyle.Scoped);
        }
    }
}