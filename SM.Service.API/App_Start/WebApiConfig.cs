﻿using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Formatting;
using System.Web.Http;

namespace SM.Service.API
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "Product",
                routeTemplate: "api/v1/product/{id}",
                defaults: new { controller = "product", id = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
                name: "Category",
                routeTemplate: "api/v1/category/{id}",
                defaults: new { controller = "category", id = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
                name: "PaymentMethod",
                routeTemplate: "api/v1/paymentmethod/{id}",
                defaults: new { controller = "paymentmethod", id = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
                name: "Basket",
                routeTemplate: "api/v1/basket/{id}",
                defaults: new { controller = "basket", id = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
                name: "BasketItem",
                routeTemplate: "api/v1/basketitem/{id}",
                defaults: new { controller = "basketitem" }
            );

            var jsonFormatter = config.Formatters.OfType<JsonMediaTypeFormatter>().FirstOrDefault();
            jsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
        }
    }
}
