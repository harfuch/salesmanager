﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SM.Service.API.Models
{
    public class ProductModel
    {
        public string URL { get; set; }
        public int ID { get; set; }
        public string ProductName { get; set; }
        public int IdCategory { get; set; }
        public decimal ProductPrice { get; set; }
    }
}