﻿using SM.Core.Entity;
using System.Linq;
using System.Net.Http;
using System.Web.Http.Routing;

namespace SM.Service.API.Models
{
    public class ModelFactory
    {
        UrlHelper _urlHelper;
        public ModelFactory(HttpRequestMessage request)
        {
            _urlHelper = new UrlHelper(request);
        }
        public ProductModel Create(ProductEntity product)
        {
            if (product != null)
                return new ProductModel()
                {
                    URL = _urlHelper.Link("Product", new { id = product.ID }),
                    ID = product.ID,
                    ProductName = product.ProductName,
                    ProductPrice = product.ProductPrice,
                    IdCategory = product.IdCategory
                };
            return null;
        }

        public CategoryModel Create(CategoryEntity category)
        {
            return new CategoryModel()
            {
                ID = category.ID,
                CategoryName = category.CategoryName
            };
        }

        public PaymentMethodModel Create(PaymentMethodEntity payment)
        {
            return new PaymentMethodModel()
            {
                ID = payment.ID,
                PaymentDescription = payment.PaymentDescription
            };
        }

        public BasketModel Create(BasketEntity basket)
        {
            return new BasketModel()
            {
                ID = basket.ID,
                IdUser = basket.IdUser,
                IP = basket.IP,
                BasketItens = basket.BasketItem.Select(e => Create(e)).ToList()
            };
        }

        public BasketItemModel Create(BasketItemEntity basketItem)
        {
            return new BasketItemModel()
            {
                ID = basketItem.ID,
                IdBasket = basketItem.IdBasket,
                IdProduct = basketItem.IdProduct,
                ItemPrice = basketItem.ItemPrice,
                ItemQuantity = basketItem.ItemQuantity
                ,                ProductItem  = Create(basketItem.ProductItem)
            };
        }

        public ProductEntity Parse(ProductModel newProduct)
        {
            try
            {
                var prod = new ProductEntity()
                {
                    IdCategory = newProduct.IdCategory,
                    ProductName = newProduct.ProductName,
                    ProductPrice = newProduct.ProductPrice
                };

                return prod;
            }
            catch
            {
                return null;
            }
        }

        public CategoryEntity Parse(CategoryModel category)
        {
            try
            {
                var cat = new CategoryEntity()
                {
                    CategoryName = category.CategoryName
                };

                return cat;
            }
            catch
            {
                return null;
            }
        }

        public PaymentMethodEntity Parse(PaymentMethodModel newPayMethod)
        {
            try
            {
                var pay = new PaymentMethodEntity()
                {
                    PaymentDescription = newPayMethod.PaymentDescription
                };

                return pay;
            }
            catch
            {
                return null;
            }
        }

        public BasketEntity Parse(BasketModel basket)
        {
            try
            {
                var bask = new BasketEntity()
                {
                    IP = basket.IP,
                    IdUser = basket.IdUser
                };

                if (basket.ID != null)
                    bask.ID = (int)basket.ID;

                if (basket.BasketItens != null)
                    bask.BasketItem = basket.BasketItens.Select(model => Parse(model, bask)).ToList();
                //bask.BasketItem = basket.BasketItens.Cast<BasketItemEntity>().ToList(); // Select(x => new BasketEntity { });

                return bask;
            }
            catch
            {
                return null;
            }
        }

        public BasketItemEntity Parse(BasketItemModel model, BasketEntity bask)
        {
            try
            {
                var item = new BasketItemEntity()
                {
                    IdBasket = model.IdBasket,
                    IdProduct = model.IdProduct,
                    ItemPrice = model.ItemPrice,
                    ItemQuantity = model.ItemQuantity,
                    ID = model.ID,
                    Basket = bask
                };

                return item;
            }
            catch
            {
                return null;
            }
        }

        public BasketItemEntity Parse(BasketItemModel model)
        {
            try
            {
                var item = new BasketItemEntity()
                {
                    IdBasket = model.IdBasket,
                    IdProduct = model.IdProduct,
                    ItemPrice = model.ItemPrice,
                    ItemQuantity = model.ItemQuantity,
                    ID = model.ID
                };

                return item;
            }
            catch
            {
                return null;
            }
        }
    }
}