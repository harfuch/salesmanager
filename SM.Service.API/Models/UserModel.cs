﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SM.Service.API.Models
{
    public class UserModel
    {
        public string Name { get; set; }
        public string Password { get; set; }
        public string Login { get; set; }
        public bool IsAdm { get; set; }
    }
}