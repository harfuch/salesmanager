﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SM.Service.API.Models
{
    public class BasketModel
    {
        public int? ID { get; set; }
        public string IP { get; set; }
        public int? IdUser { get; set; }
        public IList<BasketItemModel> BasketItens { get; set; }
    }

    public class BasketItemModel
    {
        public int ID { get; set; }
        public int IdBasket { get; set; }
        public int IdProduct { get; set; }
        public ProductModel ProductItem { get; set; }
        public int ItemQuantity { get; set; }
        public decimal ItemPrice { get; set; }
    }
}