﻿using SM.Core.DataAccessLayer.Context;
using SM.Core.Entity.Base;
using SM.Core.Interface.DataAccessLayer.Base;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SM.Core.DataAccessLayer.Base
{
    public abstract class BaseDataAccess<T> : IBaseDataAccess<T> where T : BaseEntity
    {
        protected DataContext _ctx = null;

        public BaseDataAccess()
        {
            _ctx = new DataContext();
        }

        public List<T> GetAll()
        {
            return _ctx.Set<T>().ToList();
        }

        public List<T> GetAll(Func<T, bool> predicate)
        {
            return GetAll().Where(predicate).ToList();
        }

        public T SingleOrDefault(Func<T, bool> predicate)
        {
            return _ctx.Set<T>().SingleOrDefault(predicate);
        }

        public bool Any(Func<T, bool> predicate)
        {
            return _ctx.Set<T>().Any(predicate);
        }

        public T Find(T key)
        {
            return _ctx.Set<T>().Find(key.ID);
        }

        public void Update(T obj)
        {
            //_ctx.Entry(obj).State = EntityState.Modified;
            _ctx.Set<T>().AddOrUpdate(obj);
            Commit();
        }

        public void Commit()
        {
            _ctx.SaveChanges();
        }

        public T Add(T obj)
        {
            _ctx.Set<T>().Add(obj);
            Commit();
            return obj;
        }

        public void Delete(Func<T, bool> predicate)
        {
            _ctx.Set<T>()
                .Where(predicate).ToList()
                .ForEach(del => _ctx.Set<T>().Remove(del));
            Commit();
        }

        public void Dispose()
        {
            if (_ctx != null)
            {
                _ctx.Dispose();
                _ctx = null;
            }
        }
    }
}
