﻿using SM.Core.DataAccessLayer.Map;
using SM.Core.Entity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SM.Core.DataAccessLayer.Context
{
    public class DataContext : DbContext
    {
        public DataContext() : base(ConfigurationManager.ConnectionStrings["SMDbContext"].ConnectionString)
        {
        }

        public virtual DbSet<BasketEntity> BasketDbSet { get; set; }
        public virtual DbSet<BasketItemEntity> BasketItemDbSet { get; set; }
        public virtual DbSet<CategoryEntity> CategoryEntityDbSet { get; set; }
        public virtual DbSet<OrderEntity> OrderEndityDbSet { get; set; }
        public virtual DbSet<OrderItemEntity> OrderItemEntityDbSet { get; set; }
        public virtual DbSet<PaymentMethodEntity> PaymentMethodEntityDbSet { get; set; }
        public virtual DbSet<ProductEntity> ProductEntityDbSet { get; set; }
        public virtual DbSet<UserEntity> UserEntityDbSet { get; set; }

        /// <summary>
        /// ???
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new UserMap());
            modelBuilder.Configurations.Add(new CategoryMap());
            modelBuilder.Configurations.Add(new ProductMap());
            modelBuilder.Configurations.Add(new PaymentMethodMap());
            modelBuilder.Configurations.Add(new OrderMap());
            modelBuilder.Configurations.Add(new OrderItemMap());
            modelBuilder.Configurations.Add(new BasketMap());
            modelBuilder.Configurations.Add(new BasketItemMap());
        }
    }
}
