namespace SM.Core.DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initialcreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Basket",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        IP = c.String(),
                        IdUser = c.Int(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.BasketItem",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        IdBasket = c.Int(nullable: false),
                        IdProduct = c.Int(nullable: false),
                        ItemQuantity = c.Int(nullable: false),
                        ItemPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Product", t => t.IdProduct, cascadeDelete: true)
                .ForeignKey("dbo.Basket", t => t.IdBasket, cascadeDelete: true)
                .Index(t => t.IdBasket)
                .Index(t => t.IdProduct);
            
            CreateTable(
                "dbo.Product",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ProductName = c.String(),
                        IdCategory = c.Int(nullable: false),
                        ProductPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Category", t => t.IdCategory, cascadeDelete: true)
                .Index(t => t.IdCategory);
            
            CreateTable(
                "dbo.Category",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        CategoryName = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Order",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        IdUser = c.Int(nullable: false),
                        IdPayment = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.PaymentMethod", t => t.IdPayment, cascadeDelete: true)
                .ForeignKey("dbo.UserTable", t => t.IdUser, cascadeDelete: true)
                .Index(t => t.IdUser)
                .Index(t => t.IdPayment);
            
            CreateTable(
                "dbo.OrderItem",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        IdOrder = c.Int(nullable: false),
                        IdProduct = c.Int(nullable: false),
                        ItemQuantity = c.Int(nullable: false),
                        ItemPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Order", t => t.IdOrder, cascadeDelete: true)
                .ForeignKey("dbo.Product", t => t.IdProduct, cascadeDelete: true)
                .Index(t => t.IdOrder)
                .Index(t => t.IdProduct);
            
            CreateTable(
                "dbo.PaymentMethod",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        PaymentDescription = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.UserTable",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Password = c.String(),
                        Login = c.String(nullable: false, maxLength: 250, fixedLength: true),
                        IsAdm = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .Index(t => t.Login, unique: true, name: "IX_UniqueUserLogin");
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Order", "IdUser", "dbo.UserTable");
            DropForeignKey("dbo.Order", "IdPayment", "dbo.PaymentMethod");
            DropForeignKey("dbo.OrderItem", "IdProduct", "dbo.Product");
            DropForeignKey("dbo.OrderItem", "IdOrder", "dbo.Order");
            DropForeignKey("dbo.BasketItem", "IdBasket", "dbo.Basket");
            DropForeignKey("dbo.BasketItem", "IdProduct", "dbo.Product");
            DropForeignKey("dbo.Product", "IdCategory", "dbo.Category");
            DropIndex("dbo.UserTable", "IX_UniqueUserLogin");
            DropIndex("dbo.OrderItem", new[] { "IdProduct" });
            DropIndex("dbo.OrderItem", new[] { "IdOrder" });
            DropIndex("dbo.Order", new[] { "IdPayment" });
            DropIndex("dbo.Order", new[] { "IdUser" });
            DropIndex("dbo.Product", new[] { "IdCategory" });
            DropIndex("dbo.BasketItem", new[] { "IdProduct" });
            DropIndex("dbo.BasketItem", new[] { "IdBasket" });
            DropTable("dbo.UserTable");
            DropTable("dbo.PaymentMethod");
            DropTable("dbo.OrderItem");
            DropTable("dbo.Order");
            DropTable("dbo.Category");
            DropTable("dbo.Product");
            DropTable("dbo.BasketItem");
            DropTable("dbo.Basket");
        }
    }
}
