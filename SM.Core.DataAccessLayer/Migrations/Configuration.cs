namespace SM.Core.DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<SM.Core.DataAccessLayer.Context.DataContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(SM.Core.DataAccessLayer.Context.DataContext context)
        {
            context.UserEntityDbSet.AddOrUpdate(x => x.Login,
                    new Entity.UserEntity() { Name = "Administrator", Login = "ADM", Password = "123", IsAdm = true });

            context.CategoryEntityDbSet.AddOrUpdate(x => x.CategoryName, new Entity.CategoryEntity() { CategoryName = "Beverages" });
            context.CategoryEntityDbSet.AddOrUpdate(x => x.CategoryName, new Entity.CategoryEntity() { CategoryName = "Foods" });

            context.SaveChanges();

            var beverages = context.CategoryEntityDbSet.SingleOrDefault(s => s.CategoryName == "Beverages");
            context.ProductEntityDbSet.AddOrUpdate(x => x.ProductName, new Entity.ProductEntity() { IdCategory = beverages.ID, ProductName = "Coke", ProductPrice = 2.00M });
            context.ProductEntityDbSet.AddOrUpdate(x => x.ProductName, new Entity.ProductEntity() { IdCategory = beverages.ID, ProductName = "Apple Juice", ProductPrice = 3.00M });
            context.ProductEntityDbSet.AddOrUpdate(x => x.ProductName, new Entity.ProductEntity() { IdCategory = beverages.ID, ProductName = "Lemonade", ProductPrice = 3.00M });

            var foods = context.CategoryEntityDbSet.SingleOrDefault(s => s.CategoryName == "Foods");
            context.ProductEntityDbSet.AddOrUpdate(x => x.ProductName, new Entity.ProductEntity() { IdCategory = foods.ID, ProductName = "Roasted Turkey Sandwich", ProductPrice = 2.00M });
            context.ProductEntityDbSet.AddOrUpdate(x => x.ProductName, new Entity.ProductEntity() { IdCategory = foods.ID, ProductName = "Bacon Temptation Omelette", ProductPrice = 2.20M });
            context.ProductEntityDbSet.AddOrUpdate(x => x.ProductName, new Entity.ProductEntity() { IdCategory = foods.ID, ProductName = "Belgian Waffle Combo", ProductPrice = 2.60M });
            context.ProductEntityDbSet.AddOrUpdate(x => x.ProductName, new Entity.ProductEntity() { IdCategory = foods.ID, ProductName = "Cupcake Pancake", ProductPrice = 2.70M });
            context.ProductEntityDbSet.AddOrUpdate(x => x.ProductName, new Entity.ProductEntity() { IdCategory = foods.ID, ProductName = "Fluffy The Snowman Pancakes", ProductPrice = 2.90M });

            context.PaymentMethodEntityDbSet.AddOrUpdate(x => x.PaymentDescription, new Entity.PaymentMethodEntity() { PaymentDescription = "Credit Card" });
            context.PaymentMethodEntityDbSet.AddOrUpdate(x => x.PaymentDescription, new Entity.PaymentMethodEntity() { PaymentDescription = "Cash" });
        }
    }
}
