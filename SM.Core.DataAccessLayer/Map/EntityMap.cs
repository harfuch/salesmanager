﻿using SM.Core.Entity;
using SM.Core.Entity.Base;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.ModelConfiguration.Configuration;

namespace SM.Core.DataAccessLayer.Map
{
    public static class MappingExtensions
    {
        public static PrimitivePropertyConfiguration IsUnique(this PrimitivePropertyConfiguration configuration, string namaUnique)
        {
            return configuration.IsRequired().HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute (namaUnique) {IsUnique = true }));
        }
    }

    public class BaseMap<T> : EntityTypeConfiguration<T> where T : BaseEntity
    {
        public BaseMap(string tableName)
        {
            this.ToTable(tableName);
            this.HasKey(b => b.ID);
        }
    }

    public class UserMap : BaseMap<UserEntity>
    {
        public UserMap() : base("UserTable")
        {
            this.Property(s => s.Login).HasMaxLength(250).IsFixedLength().IsUnique("IX_UniqueUserLogin");
        }
    }

    public class CategoryMap : BaseMap<CategoryEntity>
    {
        public CategoryMap() : base("Category")
        {
        }
    }

    public class ProductMap : BaseMap<ProductEntity>
    {
        public ProductMap() : base("Product")
        {
            this.HasRequired<CategoryEntity>(c => c.ProductCategory)
                .WithMany()
                .HasForeignKey(c => c.IdCategory);
        }
    }

    public class PaymentMethodMap : BaseMap<PaymentMethodEntity>
    {
        public PaymentMethodMap() : base("PaymentMethod")
        {
        }
    }

     class OrderMap : BaseMap<OrderEntity>
    {
        public OrderMap() : base("Order")
        {
            this.HasRequired<UserEntity>(c => c.User)
                .WithMany()
               .HasForeignKey(c => c.IdUser);

            this.HasRequired<PaymentMethodEntity>(c => c.PaymentMethod)
                .WithMany()
               .HasForeignKey(c => c.IdPayment);
        }
    }
    public class OrderItemMap : BaseMap<OrderItemEntity>
    {
        public OrderItemMap() : base("OrderItem")
        {
            this.HasRequired<OrderEntity>(c => c.Order)
                .WithMany(c => c.OrderItemList)
               .HasForeignKey(c => c.IdOrder);

            this.HasRequired<ProductEntity>(c => c.ProductItem)
                .WithMany()
                .HasForeignKey(c => c.IdProduct);
        }
    }

    public class BasketMap : BaseMap<BasketEntity>
    {
        public BasketMap() : base("Basket")
        {
            this.HasMany<BasketItemEntity>(fs => fs.BasketItem)
                .WithRequired(fsi => fsi.Basket)
                .HasForeignKey(fsi => fsi.IdBasket);
        }
    }

    public class BasketItemMap : BaseMap<BasketItemEntity>
    {
        public BasketItemMap() : base("BasketItem")
        {
            this.HasRequired<BasketEntity>(c => c.Basket)
                .WithMany(i => i.BasketItem)
               .HasForeignKey(i => i.IdBasket);

            this.HasRequired<ProductEntity>(c => c.ProductItem)
                .WithMany()
                .HasForeignKey(c => c.IdProduct);
        }
    }
}
