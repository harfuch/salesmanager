﻿using SM.Core.DataAccessLayer.Base;
using SM.Core.Entity;
using SM.Core.Interface.DataAccessLayer;

namespace SM.Core.DataAccessLayer
{
    public class BasketDataAccess : BaseDataAccess <BasketEntity>, IBasketDataAccess
    {
    }
}
