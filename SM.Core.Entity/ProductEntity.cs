﻿using SM.Core.Entity.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SM.Core.Entity
{
    public class ProductEntity :  BaseEntity
    {
        public string ProductName { get; set; }
        public int IdCategory { get; set; }
        public CategoryEntity ProductCategory { get; set; }
        public decimal ProductPrice { get; set; }
    }
}
