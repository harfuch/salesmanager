﻿using SM.Core.Entity.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SM.Core.Entity
{
    public class BasketEntity : BaseEntity
    {
        public virtual ICollection<BasketItemEntity> BasketItem { get; set; }
        public string IP { get; set; }
        public int? IdUser { get; set; }
        public decimal Sum()
        {
            if (BasketItem != null)
                return BasketItem.Sum(s => s.TotalItemPrice);

            return 0;
        }
    }
}
