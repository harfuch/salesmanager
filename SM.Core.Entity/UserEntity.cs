﻿using SM.Core.Entity.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace SM.Core.Entity
{
    public class UserEntity  : BaseEntity
    {
        public string Name { get; set; }
        public string Password { get; set; }

        //[Index("IX_UniqueName", IsUnique = true)]
        public string Login { get; set; }
        public bool IsAdm { get; set; }
    }
}
