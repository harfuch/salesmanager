﻿using SM.Core.Entity.Base;
using System.Collections.Generic;
using System.Linq;

namespace SM.Core.Entity
{
    public class OrderEntity : BaseEntity
    {
        public int IdUser { get; set; }
        public UserEntity User { get; set; }

        public int IdPayment { get; set; }
        public PaymentMethodEntity PaymentMethod { get; set;}

        public IList<OrderItemEntity> OrderItemList { get; set; }

        public decimal Sum()
        {
            if (OrderItemList != null)
                return OrderItemList.Sum(s => s.TotalItemPrice);
            return 0;
        }
    }
}