﻿using SM.Core.Entity.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SM.Core.Entity
{
    public class PaymentMethodEntity : BaseEntity
    {
        public string PaymentDescription { get; set; }
    }
}
