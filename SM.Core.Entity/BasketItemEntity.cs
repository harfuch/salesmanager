﻿using SM.Core.Entity.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SM.Core.Entity
{
    public class BasketItemEntity : BaseItemEntity
    {
        public int IdBasket { get; set; }
        public virtual BasketEntity Basket { get; set; }
        
    }
}
