﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SM.Core.Entity.Base
{
    public abstract class BaseItemEntity : BaseEntity
    {
        public int IdProduct { get; set; }
        public virtual ProductEntity ProductItem { get; set; }
        public virtual int ItemQuantity { get; set; }
        public virtual decimal ItemPrice { get; set; }
        public virtual decimal TotalItemPrice
        {
            get
            {
                return ItemPrice * ItemQuantity;
            }
        }
    }
}
