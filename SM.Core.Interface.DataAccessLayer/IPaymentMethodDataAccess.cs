﻿using SM.Core.Entity;
using SM.Core.Entity.Base;
using SM.Core.Interface.DataAccessLayer.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SM.Core.Interface.DataAccessLayer
{
    public interface IPaymentMethodDataAccess : IBaseDataAccess<PaymentMethodEntity>
    {
    }
}
