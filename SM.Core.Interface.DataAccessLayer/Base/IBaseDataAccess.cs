﻿using SM.Core.Entity.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SM.Core.Interface.DataAccessLayer.Base
{
    public interface IBaseDataAccess<T> : IDisposable where T : BaseEntity
    {
        List<T> GetAll();

        List<T> GetAll(Func<T, bool> predicate);

        T SingleOrDefault(Func<T, bool> predicate);

        T Find(T obj);
        bool Any(Func<T, bool> predicate);

        void Update(T obj);

        void Commit();

        T Add(T obj);

        void Delete(Func<T, bool> predicate);
    }
}
