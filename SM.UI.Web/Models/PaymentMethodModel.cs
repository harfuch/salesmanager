﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SM.UI.Web.Models
{
    public class PaymentMethodModel
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "Payment Description is Required")]
        [DisplayName("Payment Description")]
        [StringLength(80)]
        public string PaymentDescription { get; set; }
    }
}