﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SM.UI.Web.Models
{
    public class BasketModel
    {
        public int? ID { get; set; }
        public string IP { get; set; }
        public int? IdUser { get; set; }
        public IList<BasketItemModel> BasketItens { get; set; }

        public decimal Total
        {
            get
            {
                if (BasketItens != null)
                    return BasketItens.Sum(x => x.ItemPrice);
                return 0;
            }

        }
    }

    public class BasketItemModel
    {
        public int ID { get; set; }
        public int IdBasket { get; set; }
        public int IdProduct { get; set; }
        public ProductModel ProductItem { get; set; }
        public int ItemQuantity { get; set; }

        decimal _ItemPrice = 0;
        public decimal ItemPrice
        {
            get
            {
                if (_ItemPrice <= 0 && ProductItem != null)
                    return ProductItem.ProductPrice;
                return 0;

            }
            set { _ItemPrice = value; }
        }

        public string ProductName
        {
            get
            {
                if (ProductItem != null)
                    return ProductItem.ProductName;
                return string.Empty;
            }
        }
    }
}