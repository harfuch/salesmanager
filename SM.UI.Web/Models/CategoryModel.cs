﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace SM.UI.Web.Models
{
    public class CategoryModel
    {
        public int? ID { get; set; }

        [Required(ErrorMessage = "Category Name is Required")]
        [DisplayName("Category")]
        [StringLength(80)]
        public string CategoryName { get; set; }
    }
}