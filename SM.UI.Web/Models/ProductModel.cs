﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SM.UI.Web.Models
{
    public class ProductModel
    {
        [HiddenInput(DisplayValue = false)]
        public string URL { get; set; }

        [HiddenInput(DisplayValue = true)]
        public int? ID { get; set; }

        [Required(ErrorMessage = "Product Name is Required")]
        [StringLength(80)]
        [DisplayName("Product Name")]
        public string ProductName { get; set; }

        [Required(ErrorMessage = "Category Name is Required")]
        [DisplayName("Category")]
        public int IdCategory { get; set; }
        //public IList<CategoryModel> CategoriesAvailable {get;set;}
        public IEnumerable<SelectListItem> CategoriesAvailable { get; set; }

        [Required(ErrorMessage = "Product Price is Required")]
        [DisplayName("Product Price")]
        [RegularExpression(@"^[0-9]+(\.[0-9]{1,2})$", ErrorMessage = "e.g. value: 1.22")]
        public decimal ProductPrice { get; set; }

        [DisplayName("Category Name")]
        public string CategoryName { get; set; }
    }
}