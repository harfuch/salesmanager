﻿using Newtonsoft.Json;
using SM.UI.Web.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace SM.UI.Web.Controllers
{
    public class DefaultController : Controller
    {
        string Baseurl = ConfigurationManager.AppSettings["WebAPIBaseURL"];
        // GET: Default
        public async Task<ActionResult> Index()
        {
            var p = await GetProds(0);
            return View(p);
        }

        [HttpPost]
        public async Task<ActionResult> Index(int IdCategory)
        {
            ViewBag.ListaCategorias = await GetCategoriesShow();
            var p = await GetProds(IdCategory);
            return View(p);
        }

        private async Task<IList<ProductModel>> GetProds(int IdCategory)
        {
            var cat = await GetCategoriesShow();
            ViewBag.ListaCategorias = cat;

            var p = await GetProducts(IdCategory);

            if (p != null && cat != null)
            {
                foreach (var item in p)
                {
                    item.CategoryName = cat.SingleOrDefault(x => x.Value == item.IdCategory.ToString()).Text;
                }
            }

            return p;
        }

        async Task<IList<ProductModel>> GetProducts(int IdCategory)
        {
            IList<ProductModel> Products = new List<ProductModel>();
            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource product using HttpClient  
                HttpResponseMessage Res;
                if (IdCategory > 0)
                    Res = await client.GetAsync(string.Format("api/v1/product/category/{0}", IdCategory.ToString()));
                else
                    Res = await client.GetAsync("api/v1/product");

                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var ProResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Product list  
                    Products = JsonConvert.DeserializeObject<List<ProductModel>>(ProResponse);
                }
            }
            return Products;
        }

        async Task<IList<CategoryModel>> GetCategories()
        {
            IList<CategoryModel> Categories = new List<CategoryModel>();
            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource product using HttpClient  
                HttpResponseMessage Res = await client.GetAsync("api/v1/category");

                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var ProResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Product list  
                    Categories = JsonConvert.DeserializeObject<List<CategoryModel>>(ProResponse);
                }
            }
            return Categories;
        }

        private async Task<IEnumerable<SelectListItem>> GetCategoriesShow()
        {
            IList<SelectListItem> _return = new List<SelectListItem>();

            var categories = await GetCategories();

            _return = categories
                .Select(x => new SelectListItem { Text = x.CategoryName, Value = x.ID.ToString() }).ToList();

            _return.Insert(0, new SelectListItem { Text = "Choose a Category", Value = "0" });

            return _return;
        }


    }
}