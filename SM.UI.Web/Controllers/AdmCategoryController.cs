﻿using Newtonsoft.Json;
using SM.UI.Web.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace SM.UI.Web.Controllers
{
    public class AdmCategoryController : Controller
    {
        string Baseurl = ConfigurationManager.AppSettings["WebAPIBaseURL"];

        // GET: AdmCategory
        public async Task<ActionResult> Index()
        {
            IList<CategoryModel> Categories = new List<CategoryModel>();
            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource product using HttpClient  
                HttpResponseMessage Res = await client.GetAsync("api/v1/category");

                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var ProResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Product list  
                    Categories = JsonConvert.DeserializeObject<List<CategoryModel>>(ProResponse);
                }
            }

            return View(Categories);
        }

        [HttpGet]
        public ActionResult Create()
        {
            var model = new CategoryModel();
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Create(CategoryModel model)
        {
            using (var httpClient = new HttpClient())
            {
                httpClient.BaseAddress = new Uri(Baseurl);
                httpClient.DefaultRequestHeaders.Accept.Clear();
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var response = await httpClient.PostAsJsonAsync("api/v1/category", model);

                if (response.IsSuccessStatusCode)
                    return RedirectToAction("Index");

            }
            return View(model);
        }

        [HttpGet]
        public async Task<ActionResult> Edit(int id)
        {
            CategoryModel Categories = new CategoryModel();
            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource product using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(string.Format("api/v1/category/{0}", id.ToString()));

                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var ProResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Product list  
                    Categories = JsonConvert.DeserializeObject<CategoryModel>(ProResponse);
                }
            }
            return View(Categories);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(CategoryModel model)
        {
            using (var httpClient = new HttpClient())
            {
                httpClient.BaseAddress = new Uri(Baseurl);
                httpClient.DefaultRequestHeaders.Accept.Clear();
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var response = await httpClient.PutAsJsonAsync("api/v1/category", model);

                if (response.IsSuccessStatusCode)
                    return RedirectToAction("Index");

            }
            return View(model);
        }

        [HttpGet]
        public async Task<ActionResult> Delete(int id)
        {
            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource product using HttpClient  
                HttpResponseMessage Res = await client.DeleteAsync(string.Format("api/v1/category/{0}", id.ToString()));

                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var ProResponse = Res.Content.ReadAsStringAsync().Result;

                }
            }
            return RedirectToAction("Index");
        }

    }
}