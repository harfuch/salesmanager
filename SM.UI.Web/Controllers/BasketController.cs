﻿using Newtonsoft.Json;
using SM.UI.Web.Common;
using SM.UI.Web.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace SM.UI.Web.Controllers
{
    public class BasketController : Controller
    {
        string Baseurl = ConfigurationManager.AppSettings["WebAPIBaseURL"];

        public ActionResult Index()
        {
            var cesta = GetBasket().Result;
            return View(cesta);
        }

        public async Task<ActionResult> Add(int id)
        {
            BasketModel basket = new BasketModel();
            int? idCesta = SessionHelper.GetIdCesta();

            basket.ID = idCesta;
            basket.BasketItens = new List<BasketItemModel>();
            basket.BasketItens.Add(new BasketItemModel()
            {
                IdProduct = id,
                ItemQuantity = 1
            });

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource product using HttpClient  
                HttpResponseMessage Res = await client.PostAsJsonAsync("api/v1/basket/", basket);

                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var ProResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Product list  
                    basket = JsonConvert.DeserializeObject<BasketModel>(ProResponse);
                }
            }
            SessionHelper.AddIdCesta((int)basket.ID);

            return RedirectToAction("Index");
        }

        public async Task<ActionResult> Delete(int id)
        {
            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource product using HttpClient  
                HttpResponseMessage Res = await client.DeleteAsync(String.Format("api/v1/basketitem/{0}", id.ToString()));

                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var ProResponse = Res.Content.ReadAsStringAsync().Result;
                }
            }

            return RedirectToAction("Index");
        }


        private async Task<BasketModel> GetBasket()
        {
            BasketModel basket = new BasketModel();
            int? idCesta = SessionHelper.GetIdCesta();

            if (idCesta == null)
                return basket;

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource product using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(string.Format("api/v1/basket/{0}", idCesta.ToString())).ConfigureAwait(false);

                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var ProResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Product list  
                    basket = JsonConvert.DeserializeObject<BasketModel>(ProResponse);
                }
            }

            return basket;
        }
    }
}