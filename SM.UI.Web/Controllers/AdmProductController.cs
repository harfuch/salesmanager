﻿using Newtonsoft.Json;
using SM.UI.Web.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace SM.UI.Web.Controllers
{
    public class AdmProductController : Controller
    {
        string Baseurl = ConfigurationManager.AppSettings["WebAPIBaseURL"];

        public async Task<ActionResult> Index()
        {
            IList<ProductModel> Products = new List<ProductModel>();
            IList<CategoryModel> Categories = new List<CategoryModel>();

            var Prod = GetProducts();
            var Cat = GetCategories();

            await Task.WhenAll(Prod, Cat);

            Products = Prod.Result;
            Categories = Cat.Result;

            if(Products != null && Categories != null)
            {
                foreach (var item in Products)
                {
                    item.CategoryName = Categories.SingleOrDefault(x => x.ID == item.IdCategory).CategoryName;
                }
            }

            return View(Products);
        }

        async Task<IList<ProductModel>> GetProducts()
        {
            IList<ProductModel> Products = new List<ProductModel>();
            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource product using HttpClient  
                HttpResponseMessage Res = await client.GetAsync("api/v1/product");

                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var ProResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Product list  
                    Products = JsonConvert.DeserializeObject<List<ProductModel>>(ProResponse);
                }
            }
            return Products;
        }

        async Task<IList<CategoryModel>> GetCategories()
        {
            IList<CategoryModel> Categories = new List<CategoryModel>();
            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource product using HttpClient  
                HttpResponseMessage Res = await client.GetAsync("api/v1/category");

                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var ProResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Product list  
                    Categories = JsonConvert.DeserializeObject<List<CategoryModel>>(ProResponse);
                }
            }
            return Categories;
        }

        private async Task<IEnumerable<SelectListItem>> GetCategoriesShow()
        {
            IList<SelectListItem> _return = new List<SelectListItem>();

            var categories = await GetCategories();

            _return = categories
                .Select(x => new SelectListItem { Text = x.CategoryName, Value = x.ID.ToString() }).ToList();

            _return.Insert(0, new SelectListItem { Text = "Choose a Category", Value = "" });

            return _return;
        }

        [HttpGet]
        public async Task<ActionResult> Create()
        {
            var model = new ProductModel();
            model.CategoriesAvailable = await GetCategoriesShow();
            return View(model);
        }

        [HttpGet]
        public async Task<ActionResult> Edit(int id)
        {
            ProductModel Product = new ProductModel();
            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource product using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(String.Format("api/v1/product/{0}", id.ToString()));

                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var ProResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Product list  
                    Product = JsonConvert.DeserializeObject<ProductModel>(ProResponse);
                }
            }

            Product.CategoriesAvailable = await GetCategoriesShow();
            return View(Product);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(ProductModel model)
        {
            using (var httpClient = new HttpClient())
            {
                httpClient.BaseAddress = new Uri(Baseurl);
                httpClient.DefaultRequestHeaders.Accept.Clear();
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var content = new ProductModelAPI()
                {
                    ID = model.ID,
                    ProductName = model.ProductName,
                    IdCategory = model.IdCategory,
                    ProductPrice = model.ProductPrice
                };

                var response = await httpClient.PutAsJsonAsync("api/v1/product", content);

                if (response.IsSuccessStatusCode)
                    return RedirectToAction("Index", "AdmProduct");

            }
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Create(ProductModel model)
        {
            using (var httpClient = new HttpClient())
            {
                httpClient.BaseAddress = new Uri(Baseurl);
                httpClient.DefaultRequestHeaders.Accept.Clear();
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var content = new ProductModelAPI()
                {
                    ID = model.ID,
                    ProductName = model.ProductName,
                    IdCategory = model.IdCategory,
                    ProductPrice = model.ProductPrice
                };

                var response = await httpClient.PostAsJsonAsync("api/v1/product", content);

                if (response.IsSuccessStatusCode)
                    return RedirectToAction("Index", "AdmProduct");

            }
            return View(model);
        }

        [HttpGet]
        public async Task<ActionResult> Delete(int id)
        {
            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource product using HttpClient  
                HttpResponseMessage Res = await client.DeleteAsync(String.Format("api/v1/product/{0}", id.ToString()));

                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var ProResponse = Res.Content.ReadAsStringAsync().Result;
                }
            }

            return RedirectToAction("Index");
        }

    }
}