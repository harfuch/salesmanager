﻿using Newtonsoft.Json;
using SM.UI.Web.Common;
using SM.UI.Web.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace SM.UI.Web.Controllers
{
    public class CheckoutController : Controller
    {
        string Baseurl = ConfigurationManager.AppSettings["WebAPIBaseURL"];
        public ActionResult Index()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            var pay = GetPaymentMehod().Result;
            items = pay.Select(x => new SelectListItem() { Value = x.ID.ToString(), Text = x.PaymentDescription }).ToList();
            ViewBag.PayMethod = items;

            var cesta = GetBasket().Result;
            return View(cesta);
        }

        private async Task<BasketModel> GetBasket()
        {
            BasketModel basket = new BasketModel();
            int? idCesta = SessionHelper.GetIdCesta();

            if (idCesta == null)
                return basket;

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource product using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(string.Format("api/v1/basket/{0}", idCesta.ToString())).ConfigureAwait(false);

                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var ProResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Product list  
                    basket = JsonConvert.DeserializeObject<BasketModel>(ProResponse);
                }
            }

            return basket;
        }

        private async Task<IList<PaymentMethodModel>> GetPaymentMehod()
        {
            IList<PaymentMethodModel> payMethod = new List<PaymentMethodModel>();
            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource product using HttpClient  
                HttpResponseMessage Res = await client.GetAsync("api/v1/paymentmethod").ConfigureAwait(false);

                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var ProResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Product list  
                    payMethod = JsonConvert.DeserializeObject<List<PaymentMethodModel>>(ProResponse);
                }
            }

            return payMethod;
        }

        public ActionResult Checkout(int PaymentId)
        {
            return View("Index");
        }


        }
}