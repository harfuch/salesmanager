﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SM.UI.Web.Common
{
    public static class SessionHelper
    {
        private const string NOME_CESTA = "BASKETKEY";
        public static int? GetIdCesta()
        {
            int id;
            if (System.Web.HttpContext.Current.Session[NOME_CESTA] == null)
                return null;
            if (int.TryParse(System.Web.HttpContext.Current.Session[NOME_CESTA].ToString(), out id))
                return id;
            return null;   
        }

        public static void AddIdCesta(int id)
        {
            System.Web.HttpContext.Current.Session[NOME_CESTA] = id;
        }
    }
}