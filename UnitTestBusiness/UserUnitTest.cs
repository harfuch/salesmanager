﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SM.Core.BusinessLayer;
using SM.Core.DataAccessLayer;
using SM.Core.Entity;
using SM.Core.Interface.BusinessLayer;
using SM.Core.Interface.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTestBusiness
{
    [TestClass]
    public class UserUnitTest
    {
        [TestMethod]
        public void AddUser()
        {
            UserEntity userEntityReturn = null;
            IUserDataAccess userAccess = new UserDataAccess();
            IUserBusiness userBusiness = new UserBusiness(userAccess);
            UserEntity userEntity = new UserEntity()
            {
                Login = "Leandro2",
                Name = "Leandro2",
                Password = "123"
            };
            userEntityReturn = userBusiness.Add(userEntity);
            Assert.IsNotNull(userEntityReturn);
        }
    }
}
