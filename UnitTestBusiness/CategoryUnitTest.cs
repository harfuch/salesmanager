﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SM.Core.BusinessLayer;
using SM.Core.DataAccessLayer;
using SM.Core.Entity;
using SM.Core.Interface.BusinessLayer;
using SM.Core.Interface.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTestBusiness
{
    [TestClass]
    public class CategoryUnitTest
    {
        private const string CATEGORY_NAME = "Categoria que nunca vai ter outra igual";
        private const string CATEGORY_NAME_UPD = "Categoria que nunca vai ter outra igual II";
        [TestMethod]
        public void AddCategory()
        {
            CategoryEntity cateEntityReturn = null;
            ICategoryDataAccess catAccess = new CategoryDataAccess();
            using (ICategoryBusiness catBusiness = new CategoryBusiness(catAccess))
            {
                CategoryEntity cateEntity = new CategoryEntity()
                {
                    CategoryName = CATEGORY_NAME
                };

                cateEntityReturn = catBusiness.Add(cateEntity);
            }
            Assert.IsNotNull(cateEntityReturn);
        }

        [TestMethod]
        public void FindCategory()
        {
            CategoryEntity cateEntityReturn = null;
            ICategoryDataAccess catAccess = new CategoryDataAccess();
            using (ICategoryBusiness catBusiness = new CategoryBusiness(catAccess))
            {
                cateEntityReturn = catBusiness.SingleOrDefault(x => x.CategoryName == CATEGORY_NAME);
            }
            Assert.IsNotNull(cateEntityReturn);
            Assert.AreEqual(CATEGORY_NAME, cateEntityReturn.CategoryName);
        }

        [TestMethod]
        public void UpdCategory()
        {
            CategoryEntity cateEntity = null;
            CategoryEntity cateEntityReturn = null;
            ICategoryDataAccess catAccess = new CategoryDataAccess();
            using (ICategoryBusiness catBusiness = new CategoryBusiness(catAccess))
            {
                cateEntity = catBusiness.SingleOrDefault(x => x.CategoryName == CATEGORY_NAME);
                cateEntity.CategoryName = CATEGORY_NAME_UPD;

                catBusiness.Update(cateEntity);

                cateEntityReturn = catBusiness.SingleOrDefault(x => x.CategoryName == CATEGORY_NAME_UPD);
            }

            Assert.IsNotNull(cateEntityReturn);
            Assert.AreEqual(CATEGORY_NAME_UPD, cateEntityReturn.CategoryName);
        }

        [TestMethod]
        public void DelCategory()
        {
            CategoryEntity cateEntity = null;
            CategoryEntity cateEntityReturn = null;
            ICategoryDataAccess catAccess = new CategoryDataAccess();
            using (ICategoryBusiness catBusiness = new CategoryBusiness(catAccess))
            {
                cateEntity = catBusiness.SingleOrDefault(x => x.CategoryName == CATEGORY_NAME_UPD);
                catBusiness.Delete(x => x.ID == cateEntity.ID);
                cateEntityReturn= catBusiness.SingleOrDefault(x => x.CategoryName == CATEGORY_NAME_UPD);
            }

            Assert.IsNull(cateEntityReturn);
        }
    }
}
