﻿using SM.Core.Entity.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SM.Core.Interface.BusinessLayer.Base
{
    public interface IBaseBusiness<T> : IDisposable where T : BaseEntity
    {
        List<T> GetAll();

        List<T> GetAll(Func<T, bool> predicate);

        T SingleOrDefault(Func<T, bool> predicate);
        bool Any(Func<T, bool> predicate);

        T Find(T obj);

        void Update(T obj);

        T Add(T obj);

        void Delete(Func<T, bool> predicate);
    }
}
