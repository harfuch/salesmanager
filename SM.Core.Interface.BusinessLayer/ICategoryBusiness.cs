﻿using SM.Core.Entity;
using SM.Core.Interface.BusinessLayer.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SM.Core.Interface.BusinessLayer
{
    public interface ICategoryBusiness : IBaseBusiness<CategoryEntity>
    {
    }
}
