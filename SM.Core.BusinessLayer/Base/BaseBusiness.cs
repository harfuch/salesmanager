﻿using SM.Core.Entity.Base;
using SM.Core.Interface.BusinessLayer.Base;
using SM.Core.Interface.DataAccessLayer.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SM.Core.BusinessLayer.Base
{
    public abstract class BaseBusiness<T> : IBaseBusiness<T> where T : BaseEntity
    {
        IBaseDataAccess<T> _context = null;
        public BaseBusiness(IBaseDataAccess<T> context)
        {
            _context = context;
        }
        public virtual T Add(T obj)
        {
            if (Valid(obj))
                return _context.Add(obj);
            return null;
        }
        public virtual bool Valid(T obj)
        {
            return true;
        }

        public virtual void Delete(Func<T, bool> predicate)
        {
            _context.Delete(predicate);
        }

        public void Dispose()
        {
            if (_context != null)
                _context.Dispose();
            _context = null;
        }

        public T Find(T obj)
        {
            return _context.Find(obj);
        }

        public List<T> GetAll()
        {
            return _context.GetAll();
        }

        public List<T> GetAll(Func<T, bool> predicate)
        {
            return _context.GetAll(predicate);
        }

        public T SingleOrDefault(Func<T, bool> predicate)
        {
            return _context.SingleOrDefault(predicate);
        }

        public bool Any(Func<T, bool> predicate)
        {
            return _context.Any(predicate);
        }

        public void Update(T obj)
        {
            //if (Valid(obj))
                _context.Update(obj);
        }

        public void Commit()
        {
            _context.Commit();
        }
    }
}
