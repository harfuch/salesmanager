﻿using SM.Core.BusinessLayer.Base;
using SM.Core.Entity;
using SM.Core.Interface.BusinessLayer;
using SM.Core.Interface.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SM.Core.BusinessLayer
{
    public class BasketBusiness : BaseBusiness<BasketEntity>, IBasketBusiness
    {
        IBasketDataAccess _context;
        public BasketBusiness(IBasketDataAccess context) : base(context)
        {
            _context = context;
        }

        public override BasketEntity Add(BasketEntity obj)
        {
            if (!base.Any(x => x.ID == obj.ID))
                return base.Add(obj);
            else
            {
                var basketBase = base.Find(obj);
                foreach (var item in obj.BasketItem)
                {
                    var itemBanco = basketBase.BasketItem.SingleOrDefault(x => x.IdProduct == item.IdProduct);
                    if (itemBanco == null)
                    {
                        basketBase.BasketItem.Add(item);
                        item.Basket = basketBase;
                    }
                    else
                    {
                        itemBanco.ItemPrice = item.ItemPrice;
                        itemBanco.ItemQuantity = item.ItemQuantity;
                    }
                }

                base.Update(basketBase);
                return basketBase;
            }
        }
    }
}

