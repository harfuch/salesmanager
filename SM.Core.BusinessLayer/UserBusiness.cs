﻿using SM.Core.Interface.BusinessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SM.Core.Entity;
using SM.Core.Interface.DataAccessLayer;
using SM.Core.Interface.BusinessLayer.Base;
using SM.Core.BusinessLayer.Base;

namespace SM.Core.BusinessLayer
{
    public class UserBusiness : BaseBusiness<UserEntity>, IUserBusiness
    {
        private IUserDataAccess _context = null;
        public UserBusiness(IUserDataAccess context) : base(context)
        {
            _context = context;
        }

        public override bool Valid(UserEntity newUser)
        {
            var check = _context.SingleOrDefault(x => string.Equals(x.Login, newUser.Login, StringComparison.OrdinalIgnoreCase));
            return check == null;
        }

        public UserEntity Authentication(UserEntity user)
        {
            var check = _context.SingleOrDefault(x => string.Equals(x.Login, user.Login, StringComparison.OrdinalIgnoreCase));
            if (string.Equals(check.Password, user.Password, StringComparison.CurrentCulture))
                return check;
            return null;
        }
    }
}
