﻿using SM.Core.BusinessLayer.Base;
using SM.Core.Entity;
using SM.Core.Interface.BusinessLayer;
using SM.Core.Interface.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SM.Core.BusinessLayer
{
    public class BasketItemBusiness : BaseBusiness<BasketItemEntity>, IBasketItemBusiness
    {
        IBasketItemDataAccess _context;
        public BasketItemBusiness(IBasketItemDataAccess context) : base(context)
        {
            _context = context;
        }
        public void DeleteItem(BasketItemEntity item)
        {
            _context.Delete(x => x.IdProduct == item.IdProduct && x.IdBasket == item.IdBasket);
        }
    }
}
