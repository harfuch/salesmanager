﻿using SM.Core.BusinessLayer.Base;
using SM.Core.Entity;
using SM.Core.Interface.BusinessLayer;
using SM.Core.Interface.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SM.Core.BusinessLayer
{
    public class PaymentMethodBusiness : BaseBusiness<PaymentMethodEntity>, IPaymentMethodBusiness
    {
        public PaymentMethodBusiness(IPaymentMethodDataAccess context) : base(context)
        {
        }

        public override bool Valid(PaymentMethodEntity obj)
        {
            return !base.Any(x => x.PaymentDescription == obj.PaymentDescription);
        }
    }
}
