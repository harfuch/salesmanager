﻿using SM.Core.BusinessLayer.Base;
using SM.Core.Entity;
using SM.Core.Interface.BusinessLayer;
using SM.Core.Interface.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SM.Core.BusinessLayer
{
    public class CategoryBusiness : BaseBusiness<CategoryEntity>, ICategoryBusiness
    {
        public CategoryBusiness(ICategoryDataAccess context):base(context)
        {
        }

        public override bool Valid(CategoryEntity obj)
        {
           return !base.Any(x => x.CategoryName == obj.CategoryName);
        }
    }
}
